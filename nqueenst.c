#ifndef _NQUEENS_
#define _NQUEENS_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

pthread_mutex_t lock;
struct param{
    int dim;
    unsigned long long fat;
    int queens;
    int **board;
    int x;
    int y;
    unsigned long long *result;
};

unsigned long long nqueens(int dim, int queens);
void * func(struct param *parametros);
unsigned long long possibility(int dim, int queens, int **board, int x, int y, unsigned long long  result);
int **newboard(int **board, int dim, int x, int y);
int fatorial(int x);
void createLog(int dim, int queen);

unsigned long long nqueens(int dim, int queens){
    int  i=0, j=0, k=0;
    unsigned long long fat;
    unsigned long long *result;
    struct param parametros[dim];
    result = calloc(sizeof(unsigned long long), 1);
    int **board = (int**)malloc(dim *sizeof(int*));
    pthread_t thread[dim];
    for (i = 0; i < dim; i++){
        board[i] = (int*) calloc(dim , sizeof(int));
        parametros[i].result = result;
    }
    if(dim < queens || queens == 0){
        return 0;
    }else{
        fat = fatorial(queens);
        for(i=0;i<dim;i++){
            //parametros[i].queens = queens;
            for(j=0;j < dim;j++){
                parametros[j].dim = dim;
                parametros[j].fat = fat;
                parametros[j].queens = queens;
                parametros[j].board = board;
                parametros[j].x = i;
                parametros[j].y = j;
                k = pthread_create( &thread[j], NULL, (void*)func, (void*)&parametros[j]);
		if(k!=0){
			printf("ERRO\n");
		}
            }
            for(j=0; j < dim; j++){
                pthread_join(thread[j], NULL);
            }
        }
        free(board);

        pthread_mutex_destroy(&lock);
        return *result/fat;
    }
}
void * func(struct param *parametros){
    unsigned long long resultf = 0;

    resultf = possibility(parametros->dim, parametros->queens,parametros->board , parametros->x, parametros->y, 0);
    pthread_mutex_lock(&lock);
    *(parametros->result) = resultf + *(parametros->result);
    pthread_mutex_unlock(&lock);
    return 0;

}
unsigned long long possibility(int dim, int queens, int **board, int x, int y, unsigned long long result){

    int i, j;
    unsigned long long temp,resultParcial, resultf = 0;
    resultParcial = result;

    int **board2 = (int**)malloc(dim *sizeof(int*));
    for (i = 0; i < dim; i++){
       board2[i] = (int*) calloc(dim , sizeof(int));
    }
    for(i=0;i<dim;i++){
        for(j=0;j<dim;j++){
            board2[i][j] = board[i][j];
        }
    }
    if(queens == 1){

        resultf = pow(2,(x*dim)+y)+result;

    }else{
    board2 = newboard(board2, dim, x, y);
    resultParcial = pow(2,(x*dim)+y)+ result;
    for (i = 1; i < dim; i++){
        for (j = 1; j < dim; j++){
                if(board2[(i+x)%dim][(j+y)%dim]==0){
                    temp = possibility(dim, queens-1, board2, (i+x)%dim, (j+y)%dim, resultParcial) ;
                    if(temp != 0){
                        resultf = resultf + temp;
                    }
                }
            }
        }
    }
    for(i=0;i<dim;i++){
        free(board2[i]);
    }
    free(board2);


    return resultf;
}

int **newboard(int **board, int dim, int x, int y){
    int i, j;
    for(i=0; i<dim; i++){
        board[x][i] = 1;
        board[i][y] = 1;
    }

    for(i=0; i<dim; i++){

         for(j=0; j<dim; j++){

             if(abs(x-i) == abs(y-j)){
                board[i][j] = 1;
             }
         }
    }
    return board;
}
int fatorial(int x){
	int i, fat;
	if(x>0){
        fat = 1;
		for(i=1;i<=x;i++){
			fat=fat*i;
		}
		return fat;
	}
	return -1;
}



#endif
